<?php

namespace App\Models\Football;

use Illuminate\Database\Eloquent\Model;

class Club extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clubs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'points',
    ];
}
