<?php

namespace App\Traits;

use App\Models\Football\Club;

trait Helper
{
    public function generatePoints($matchScore)
    {
        // Explode the score
        $score = explode(' ', $matchScore);
        // - Winner get 3 points
        // - Draw get 1 point
        // - Lose get 0 point

        $homeScore = 0;
        $awayScore = 0;
        if ($score[0] > $score[2]) {
            // Home Menang - Away Kalah
            $homeScore = 3;
            $awayScore = 0;
        } else if ($score[0] == $score[2]) {
            // Draw
            $homeScore = 1;
            $awayScore = 1;
        } else if ($score[0] < $score[2]) {
            // Home Kalah - Away Menang
            $homeScore = 0;
            $awayScore = 3;
        }


        return [
            'home_score' => $homeScore,
            'away_score' => $awayScore
        ];
    }

    public function validateClub($club, $score)
    {
        $getClub = Club::whereRaw('LOWER(name) = ?', [strtolower($club)])->first();

        $result = [
            'message' => '',
            'points' => 0,
        ];

        if ($getClub) {
            $points = $getClub->points + $score;
            $result['points'] = $points;
        } else {
            $result['message'] = 'Club dengan nama : ' . $club . ' tidak ditemukan';
        }

        return $result;
    }

    public function addPointToClub($club, $point)
    {
        Club::where('name', $club)->update([
            'points' => $point
        ]);
    }
}
