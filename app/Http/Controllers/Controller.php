<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Traits\Helper;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, Helper;

    protected function errorResponse($message, $code, $data = null)
    {
        return response()->json(['meta' => ['code' => $code, 'status' => 'error', 'message' => $message], 'data' => $data], $code);
    }

    protected function successResponse($message, $data, $code)
    {
        return response()->json(['meta' => ['code' => $code, 'status' => 'success', 'message' => $message], 'data' => $data], $code);
    }
}
