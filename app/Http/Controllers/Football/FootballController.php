<?php

namespace App\Http\Controllers\Football;

use Illuminate\Http\Request;
use App\Models\Football\Club;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClubResource;
use App\Http\Resources\ClubPointResource;

class FootballController extends Controller
{
    public function club()
    {
        $club = Club::all();

        return $this->successResponse('Berhasil Menampilkan Semua Club', ClubResource::collection($club), 200);
    }

    public function recordGame(Request $request)
    {
        $data = [];
        $matches = $request->all();
        foreach ($matches as $match) {

            // * Generate Points
            $score = $this->generatePoints($match['score']);

            // * Validate the club name
            $homeClub = $this->validateClub($match['clubhomename'], $score['home_score']);
            if (!empty($homeClub['message'])) {
                return $this->errorResponse($homeClub['message'], 400);
            }

            // * Validate the club name
            $awayClub = $this->validateClub($match['clubawayname'], $score['away_score']);
            if (!empty($awayClub['message'])) {
                return $this->errorResponse($awayClub['message'], 400);
            }

            $this->addPointToClub($match['clubhomename'], $homeClub['points']);
            $this->addPointToClub($match['clubawayname'], $awayClub['points']);

        }

        return $this->successResponse('Berhasil Memperbaharui Data', [], 200);
    }

    public function leagueStanding()
    {
        $club = Club::orderBy('points', 'desc')->get();

        return $this->successResponse('Berhasil Mendapatkan Data', ClubPointResource::collection($club), 200);
    }

    public function rank(Request $request)
    {
        $clubs = Club::query();
        $clubName = $request->query('clubname');
        if (isset($clubName)) {
            $clubs->where('name', 'ILIKE', '%' . $clubName . '%');
        }
        $clubs = $clubs->orderBy('points', 'desc')->get();

        if (count($clubs) == 0) {
            return $this->errorResponse('Data tidak ditemukan', 400);
        }

        $result = [];
        foreach ($clubs as $key => $value) {
            array_push($result, [
                'clubname' => $value->name,
                'standing' => $key+1
            ]);
        }

        return $this->successResponse('Berhasil Mendapatkan Ranking Club', $result, 200);
    }
}
