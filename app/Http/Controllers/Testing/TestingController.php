<?php

namespace App\Http\Controllers\Testing;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestingController extends Controller
{
    public function isContainLetters(Request $request)
    {
        if (strpos(strtolower($request->second_word), strtolower($request->first_word)) !== false) {
            return $this->successResponse('', true, 200);
        } else {
            return $this->successResponse('', false , 200);
        }
    }
}
