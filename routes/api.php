<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('football/club', 'Football\FootballController@club');
Route::get('football/leaguestanding', 'Football\FootballController@leagueStanding');
Route::get('football/rank', 'Football\FootballController@rank');
Route::post('football/recordgame', 'Football\FootballController@recordGame');
Route::post('is-contain-letters', 'Testing\TestingController@isContainLetters');

