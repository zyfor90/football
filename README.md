```
Run Program
-Composer Install
-php artisan key:generate
-php artisan migrate
-php artisan db:seed
-php artisan serv
```

```
Note: Tadi saya bikin 2 projects. 1 menggunakan laravel 1 lagi menggunakan golang, 
untuk link project menggunakan Golang ada disini
https://gitlab.com/zyfor90/football-go

Link Dokumentasi API
https://documenter.getpostman.com/view/11540468/2s935vmgC8

Link Postman Collection
https://api.postman.com/collections/11540468-407e9e01-2b93-46b4-9b74-eccca0cf2b17?access_key=PMAT-01GS80ABJ8KS4R5P97C1ED00WA
```

```

Note: Untuk Soal Query Table mobil cara sudah mencoba menggunakan beberapa cara seperti ini tapi blm bisa memunculkan table seperti di result jadi saya mencoba untuk menggerjakan soal API terlebih dahulu. 

Cara Pertama: 
SELECT brand, MAX(CASE WHEN type = 'civic' THEN type || ' : ' || price END) AS type1, MAX(CASE WHEN type = 'jazz' THEN type || ' : ' || price END) AS type2, MAX(CASE WHEN type = 'mobilio' THEN type || ' : ' || price END) AS type3 FROM cars GROUP BY brand;

Cara Kedua:
with grouped as (
with h as (select *, row_number() over (order by id) from vehicle where brand = 'honda'),
 d as (select *, row_number() over (order by id) from vehicle where brand = 'daihatsu'),
 t as (select *, row_number() over (order by id) from vehicle where brand = 'toyota')
select * from h union select * from d union select * from t
order by row_number)
select brand,
(case when row_number = 1 then type || ' : ' || price END) as type1,
(case when row_number = 2 then type || ' : ' || price END) as type2,
(case when row_number = 3 then type || ' : ' || price END) as type3
from grouped
group by brand, grouped.row_number, grouped.type,grouped.price

Cara Ketika: 
SELECT t1.brand, t1.type || ' : ' || t1.price AS type1, t2.type || ' : ' || t2.price AS type2, t3.type || ' : ' || t3.price AS type3
FROM cars t1
LEFT JOIN cars t2
ON t1.brand = t2.brand and t1.id < t2.id
LEFT JOIN cars t3
ON t2.brand = t3.brand and t2.id < t3.id
WHERE t1.id < t2.id
GROUP BY t1.brand, t1.id, t1.type, t1.price, t2.type, t2.price, t3.type, t3.price
ORDER BY t1.id;                                                           

```
